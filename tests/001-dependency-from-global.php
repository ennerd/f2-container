<?php
namespace {
    require(__DIR__."/../vendor/autoload.php");
}
namespace Frode\Er\Stilig {
    use function F2\asserty;
    use F2;
    function someFunction() {
        asserty(F2::container()->has('container'));
    }

    $container = F2::container()->get('container');

    asserty(get_class($container) === F2\Container\Container::class);

    someFunction();
}
