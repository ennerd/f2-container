<?php
declare(strict_types=1);

namespace F2\Container;

use League\Container\Container as LeagueContainer;
use Psr\Container\ContainerInterface;

/**
 * This class is final, because there will never be a proper way to override it
 * since it forms the basis for everything in F2. We may implement a way to override the
 * instance that is returned from getInstance()
 */
final class Container extends LeagueContainer implements Contracts\RootContainerInterface {

    /**
     * {@inheritdoc}
     */
    public static function getInstance(): Contracts\RootContainerInterface {
        static $instance;
        if (!$instance) {
            $instance = new static();
            $instance->addServiceProvider(new ManifestServiceProvider());
        }
        return $instance;
    }

    /**
     * {@inheritdoc}
     */
    public function autowire($wireable, array $vars=[], array $instances=[]): \Closure {
        if (is_callable($wireable)) {
            return function() use ($wireable, $vars, $instances) {
                $args = $this->prepareArgumentsForCallable($wireable, $vars, $instances);
                return $wireable(...$args);
            };
        } elseif (class_exists($wireable)) {
            return function() use ($wireable, $vars, $instances) {
                $args = $this->prepareArgumentsForCallable([ $wireable, '__construct' ], $vars, $instances);
                return new $wireable(...$args);
            };
        } else {
            throw new AutowiringException("Unable to auto-wire '$wireable' because it is neither a callable nor a class name.");
        }
    }

    protected function prepareArgumentsForCallable(callable $callable, array $vars, array $instances): array {
        $handler = \Closure::fromCallable($callable);
        $ref = new \ReflectionFunction($handler);
        $identity = "[".$ref->getName()." file=".$ref->getFileName()." line=".$ref->getStartLine()."]";

        $args = [];

        foreach ($ref->getParameters() as $param) {
            $type = $param->hasType() ? $param->getType()->__toString() : 'mixed';

            /**
             * Handle named vars
             */
            if (isset($vars[$param->name])) {
                // The value comes from $vars
                if ($type === 'mixed') {
                    $args[] = $vars[$param->name];
                    continue;
                } elseif ($type === 'string') {
                    $args[] = ''.$vars[$param->name];
                    continue;
                } elseif ($type === 'integer') {
                    $args[] = intval($vars[$param->name]);
                    continue;
                } elseif ($type === 'double' || $type == 'float') {
                    $args[] = floatval($vars[$param->name]);
                    continue;
                } elseif ($type === 'boolean') {
                    $args[] = !!$vars[$param->name];
                    continue;
                } else {
                    throw new AutowiringException("Could not auto-wire '$identity' because the argument \$".$param->name." provided via variables expects type unsupported type '$type'");
                }
            }

            /**
             * Untyped with default values or that allows null
             */
            if (!$param->hasType()) {
                if ($param->isDefaultValueAvailable()) {
                    $args[] = $param->getDefaultValue();
                    continue;
                }
                if ($param->allowsNull()) {
                    $args[] = null;
                    continue;
                }
                throw new AutowiringException("Could not auto-wire '$identity' because the argument \$".$param->name." has no type hint and the value is not provided via variables");
            }

            /**
             * Typed provided by $instances
             */
            foreach ($instances as $instance) {
                if ($instance instanceof $type) {
                    $args[] = $instance;
                    continue 2;
                }
            }

            /**
             * Types supported by container
             */
            if ($this->has($type)) {
                $args[] = $this->get($type);
                continue;
            }

            /**
             * Unprovided type, but with default value
             */
            if ($param->isDefaultValueAvailable()) {
                $args[] = $param->getDefaultValue();
                continue;
            }

            /**
             * Allows null
             */
            if ($param->allowsNull()) {
                $args[] = null;
                continue;
            }

            /**
             * Optional arguments is the end of arguments so we stop
             */
            if ($param->isOptional()) {
                break;
            }

            throw new AutowiringException("Could not auto-wire '$identity', unable to infer value for argument \$".$param->name);
        }

        return $args;
    }

    /**
     * {@inheritdoc}
     */
    public function addFromSpecification( array $spec ): void {
throw new Exception("NOT USED");
        // Simple validation
        if (!isset($spec[0]) || !is_string($spec[0])) {
            throw new NotSupportedException("Service specification must be an array in the format [string \$type, ...\$parameters]");
        }
        switch ($spec[0]) {
            case 'callable' : // ['callable', callable $callback, string $identifier, bool $shared=false]
                if (!(
                    // string, callable, string, bool
                    (isset($spec[3]) && is_bool($spec[3]) && is_string($spec[2]) && is_callable($spec[1])) ||
                    // string, callable, string
                    (isset($spec[2]) && is_string($spec[2]) && is_callable($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'callable' must be in format ['callable' callable \$callback, string \$identifier, bool $shared=false]");
                }
                $this->addCallableFactory( $spec[1], $spec[2], !!($spec[3] ?? false) );
                break;
            case 'factory' :
                if (!isset($spec[1]) || !($spec[1] instanceof Contracts\FactoryInterface)) {
                    throw new NotSupportedException("Service specification for 'factory' must be in format ['factory', ".Contracts\FactoryInterface::class." \$factory]");
                }
                $this->addFactory( $spec[1] );
                break;
            case 'file' : // ['file', string $path, string $identifier, bool $shared=false]
                if (!(
                    // string, string, string, bool
                    (isset($spec[3]) && is_bool($spec[3]) && is_string($spec[2]) && is_string($spec[1])) ||
                    (isset($spec[2]) && is_string($spec[2]) && is_string($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'file' must be in format ['file', string \$path, string \$identifier, bool \$shared=false]");
                }
                $this->addFileFactory( $spec[1], $spec[2], !!($spec[3] ?? false) );
                break;
            case 'class' : // ['class', string $className[, string $identifier], bool $shared=false]
                if (!(
                    // string, string, string, bool
                    (isset($spec[3]) && is_bool($spec[3]) && is_string($spec[2]) && is_string($spec[1])) ||
                    // string, string, bool
                    (isset($spec[2]) && is_bool($spec[2]) && is_string($spec[1])) ||
                    // string, string, string
                    (isset($spec[2]) && is_string($spec[2]) && is_string($spec[1])) ||
                    // string, string
                    (isset($spec[1]) && is_string($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'class' must be in format ['class', string \$className[, string \$identifier], bool \$shared=false]");
                }
                if (!class_exists($spec[1])) {
                    throw new NotFoundException("Service specification declares a non-existent class '".$spec[1]."'");
                }
                $className = $spec[1];
                $identifier = is_string($spec[2]) ? $spec[2] : $spec[1];
                $shared = isset($spec[3]) ? $spec[3] : (is_bool($spec[2]) ? $spec[2] : false);
                $this->addClassFactory( $className, $identifier, $shared );
                break;
            case 'instance' : // ['instance', object $instance[, string $identifier]]
                if (!(
                    // string, object, string
                    (isset($spec[2]) && is_string($spec[2]) && is_object($spec[1])) ||
                    // string, object
                    (isset($spec[1]) && is_object($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'instance' must be in format ['instance', object \$instance[, string \$identifier]]");
                }
                $identifier = isset($spec[2]) ? $spec[2] : get_class($spec[1]);
                $this->addInstanceFactory( $spec[1], $identifier );
                break;
            default :
                throw new NotSupportedException("Service specification type '".$spec[0]."' is unknown.");
        }
    }

}
