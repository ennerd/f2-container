<?php
declare(strict_types=1);
namespace F2\Container;
use League\Container\Definition\Definition;

/**
 * Factory for services that are supplied by the framework. Configuration of
 * factories should be done in files under config/*
 */
abstract class AbstractFactory {

    protected $_definition;

    /**
     * Create an instance of this factory
     *
     * @return AbstractFactory
     */
    public static function getInstance(): AbstractFactory {
        return new static();
    }

    /**
     * The primary identifier for the service that this class returns. Normally
     * this will be the class name of the object. Any interfaces and parent classes
     * of the provided class name will automatically be added as "weak aliases".
     *
     * Weak aliases can be overwritten by services providers, and can't override
     * any existing strong aliases.
     *
     * Returns an identifier according to {@see Psr\Container\ContainerInterface::get()}
     */
    abstract public function getIdentifier(): string;

    /**
     * Return a list of identifiers that this service provides, and that other
     * providers would be in conflict with.
     */
    public function provides(): array {
        return [ $this->getIdentifier() ];
    }

    /**
     * Create the instance according to the customer configuration and return it.
     * The resulting service MUST be an instance of $this->getIdentifier() or
     * in the list returned from $this->provides().
     */
    abstract public function createInstance();

    /**
     * Multiple consumers can share the same instance of this class.
     */
    public function isShared(): bool {
        return true;
    }

    public function getDefinition(): Definition {
        if (!$this->_definition) {
            $this->_definition = new Definition(
                $this->getIdentifier(),
                [ $this, 'createInstance' ]
            );
            $this->_definition->setShared($this->isShared());
        }
        return $this->_definition;
    }
}
