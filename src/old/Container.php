<?php
declare(strict_types=1);
namespace F2\Container;

use F2;
use Closure;
use ReflectionFunction;
use League\Container\Container as LeagueContainer;
use Psr\Container\ContainerInterface as PsrContainerInterface;
use F2\Common\MissingConfigException;
use F2\Common\Optimizer;
use function debug_backtrace;

/**
 * Note, this Container is not intended to be used externally - it has
 * methods that are very specific for the F2 framework.
 */
class Container implements ContainerInterface {
    /**
     * The internal service container that implements the actual
     * service container
     */
    protected $container;

    /**
     * List of service identifiers that have been registered
     */
    protected $identifiers = [];

    /**
     * A graph of aliases from interface/parentClass => service id
     */
    protected $aliases = [];

    /**
     * Constructor.
     */
    public function __construct() {
        $this->container = new LeagueContainer();
    }

    /**
     * Retrieve the service according to service id
     *
     * @see \Psr\Container\ContainerInterface::get()
     *
     * @param mixed $id         The service id
     * @param mixed[] $args     Arguments for the constructor
     */
    public function get( $id, ...$args ) {
        if ($this->container->has( $id )) {
            return $this->container->get( $id );
        }

        if (!isset($this->aliases[$id])) {
            throw new NotFoundException("Service '$id' not provided by the Service Container.");
        }

        $candidates = [];
        $queue = $this->aliases[$id];
        while(null !== ($next = array_shift($queue))) {
            if ($this->container->has($next)) {
                $candidates[] = $next;
            }
            if (isset($this->aliases[$next])) {
                foreach ($this->aliases[$next] as $alias) {
                    $queue[] = $alias;
                }
            }
        }
        $candidates = array_unique($candidates, SORT_STRING);
        if (sizeof($candidates) > 1) {
            throw new MultipleCandidatesException($candidates);
        }
        return $this->container->get(array_shift($candidates));
    }

    /**
     * Check if the the service id is registered.
     *
     * @see \Psr\Container\ContainerInterface::has()
     *
     * @param mixed $id     The service id
     */ 
    public function has( $id ) {
        if ($this->container->has( $id ) || isset($this->aliases[$id])) {
            // If we have an alias, then we must have an implementation
            return true;
        }
        return false;
    }

    /**
     * Register a service container as a delegate, which means that any services that are not found
     * in this container will be fetched from the delegate container.
     *
     * @param PsrContainerInterface $container  The service container instance
     */
    public function delegate(PsrContainerInterface $container): void {
        $this->container->delegate($container);
    }

    /**
     * Register an AbstractFactory as a service provider.
     *
     * @param AbstractFactory $factory      The factory instance
     */
    public function addFactory( AbstractFactory $factory ): void {
        $this->container->add($factory->getIdentifier(), [$factory, 'createInstance'], $factory->isShared());
        $this->registerIdentifier($factory->getIdentifier());
    }

    /**
     * Convenience method for creating a service from a service specification array. A
     * service specification array has the following form:
     *
     * ```php
     * [ $type, $typeParameter, $identifier, $shared ]
     * ```
     *
     * $type must be one of 'callable', 'factory', 'file', 'class', 'instance'
     *
     * Examples:
     * [ 'callable', [ SomeClass::class, 'some_method' ], MyInterface::class, true ]
     *     - Register a callback as a factory
     *
     * [ 'factory', \F2\Container\AbstractFactory $factory ]
     *     - Register a service factory instance
     *
     * [ 'file', 'path/to/file.php', MyInterface::class, true ]
     *     - Will include the file and use whatever value is returned by that file
     *
     * [ 'class', SomeClass::class, MyInterface::class, true ]
     *     - Will instantiate the class for you
     *
     * [ 'instance', $instance ]
     *     - This instance is always shared
     *
     * $typeParameter must be:
     *     - an array or a string containing a callable (NOT a closure)
     *     - a configuration key,
     *     - a factory class name,
     *     - a file path (relative paths are resolved from APP_ROOT)
     *
     * $identifier is the interface name or any other string that serves as an identifier
     *
     * $shared is a boolean: true means that the instance is cached and reused
     *
     * @param array $spec The service specification (described above)
     */
    public function addFactoryFromSpecification( array $spec ): void {
        // Simple validation
        if (!isset($spec[0]) || !is_string($spec[0])) {
            throw new NotSupportedException("Service specification must be an array in the format [string \$type, ...\$parameters]");
        }
        switch ($spec[0]) {
            case 'callable' : // ['callable', callable $callback, string $identifier, bool $shared=false]
                if (!(
                    // string, callable, string, bool
                    (isset($spec[3]) && is_bool($spec[3]) && is_string($spec[2]) && is_callable($spec[1])) ||
                    // string, callable, string
                    (isset($spec[2]) && is_string($spec[2]) && is_callable($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'callable' must be in format ['callable' callable \$callback, string \$identifier, bool $shared=false]");
                }
                $this->addCallableFactory( $spec[1], $spec[2], !!($spec[3] ?? false) );
                break;
            case 'factory' :
                if (!isset($spec[1]) || !($spec[1] instanceof Contracts\FactoryInterface)) {
                    throw new NotSupportedException("Service specification for 'factory' must be in format ['factory', ".Contracts\FactoryInterface::class." \$factory]");
                }
                $this->addFactory( $spec[1] );
                break;
            case 'file' : // ['file', string $path, string $identifier, bool $shared=false]
                if (!(
                    // string, string, string, bool
                    (isset($spec[3]) && is_bool($spec[3]) && is_string($spec[2]) && is_string($spec[1])) ||
                    (isset($spec[2]) && is_string($spec[2]) && is_string($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'file' must be in format ['file', string \$path, string \$identifier, bool \$shared=false]");
                }
                $this->addFileFactory( $spec[1], $spec[2], !!($spec[3] ?? false) );
                break;
            case 'class' : // ['class', string $className[, string $identifier], bool $shared=false]
                if (!(
                    // string, string, string, bool
                    (isset($spec[3]) && is_bool($spec[3]) && is_string($spec[2]) && is_string($spec[1])) ||
                    // string, string, bool
                    (isset($spec[2]) && is_bool($spec[2]) && is_string($spec[1])) ||
                    // string, string, string
                    (isset($spec[2]) && is_string($spec[2]) && is_string($spec[1])) ||
                    // string, string
                    (isset($spec[1]) && is_string($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'class' must be in format ['class', string \$className[, string \$identifier], bool \$shared=false]");
                }
                if (!class_exists($spec[1])) {
                    throw new NotFoundException("Service specification declares a non-existent class '".$spec[1]."'");
                }
                $className = $spec[1];
                $identifier = is_string($spec[2]) ? $spec[2] : $spec[1];
                $shared = isset($spec[3]) ? $spec[3] : (is_bool($spec[2]) ? $spec[2] : false);
                $this->addClassFactory( $className, $identifier, $shared );
                break;
            case 'instance' : // ['instance', object $instance[, string $identifier]]
                if (!(
                    // string, object, string
                    (isset($spec[2]) && is_string($spec[2]) && is_object($spec[1])) ||
                    // string, object
                    (isset($spec[1]) && is_object($spec[1]))
                )) {
                    throw new NotSupportedException("Service specification for 'instance' must be in format ['instance', object \$instance[, string \$identifier]]");
                }
                $identifier = isset($spec[2]) ? $spec[2] : get_class($spec[1]);
                $this->addInstanceFactory( $spec[1], $identifier );
                break;
            default :
                throw new NotSupportedException("Service specification type '".$spec[0]."' is unknown.");
        }
    }


    public function addClassFactory( string $className, string $identifier, bool $shared=false): void {
        $this->addFactory( new class($className, $identifier, $shared) extends AbstractFactory {
            protected $className;
            protected $identifier;
            protected $shared;

            public function __construct(string $className, string $identifier, bool $shared) {
                $this->className = $className;
                $this->identifier = $identifier;
                $this->shared = $shared;
            }

            public function getIdentifier(): string {
                return $this->identifier;
            }

            public function createInstance(...$args) {
                $className = $this->className;
                return new $className(...$args);
            }

            public function isShared(): bool {
                return $this->shared;
            }
        } );
    }


    public function addInstanceFactory( string $className, string $identifier): void {
        $this->addFactory( new class($instance, $identifier) extends AbstractFactory {
            protected $instance;
            protected $identifier;

            public function __construct(string $instance, string $identifier) {
                $this->instance = $instance;
                $this->identifier = $identifier;
            }

            public function getIdentifier(): string {
                return $this->identifier;
            }

            public function createInstance() {
                return $this->instance;
            }

            public function isShared(): bool {
                // We don't want container to also cache the instance.
                return false;
            }
        } );
    }


    /**
     * Convenience method for creating a factory from a PHP file. The file will be included
     * and the return value will be returned as the service.
     *
     * @param string $path          Path to PHP file
     * @param string $identifier    Service identifier
     * @param bool $shared          Should we cache the result from the factory and share it?
     */
    public function addFileFactory( string $path, $identifier, bool $shared=false ): void {

        $this->addFactory( new class($path, $identifier, $shared) extends AbstractFactory {
            protected $path;
            protected $identifier;
            protected $shared;

            public function __construct(string $path, string $identifier, bool $shared) {
                $this->path = $path;
                $this->identifier = $identifier;
                $this->shared = $shared;
            }

            public function getIdentifier(): string {
                return $this->identifier;
            }

            public function createInstance() {
                return require($this->path);
            }

            public function isShared(): bool {
                return $this->shared;
            }
        } );

        $this->registerIdentifier($identifier);
    }

    /**
     * Convenience method for creating a factory from a callable without creating a new Factory class
     *
     * @param callable $factory The callback that will return the service
     * @param bool $shared      Should we cache the result from the factory and share it?
     */
    public function addCallableFactory( callable $factory, $identifier, bool $shared=false ): void {
        $factory = Closure::fromCallable( $factory );
        $reflection = new ReflectionFunction( $factory );
        if( !$reflection->hasReturnType() ) {
            throw new NotSupportedException("Factories must type hint their return type. Add :className to your factory, or wrap it in a closure. (identifier=$identifier)");
        }

        $this->addFactory( new class($factory, $identifier, $shared) extends AbstractFactory {
            public function __construct(callable $factory, $identifier, bool $shared=false) {
                $this->factory = $factory;
                $this->identifier = $identifier;
                $this->shared = $shared;
            }

            public function getIdentifier(): string {
                if ($this->identifier !== null) {
                    return $this->identifier;
                }

                $reflection = new ReflectionFunction( $this->factory );
                if( !$reflection->hasReturnType() ) {
                    throw new NotSupportedException("Factories must type hint their return type. Add :className to your factory, or wrap it in a closure.");
                }

                return $reflection->getReturnType()->getName();
            }

            public function createInstance() {
                return call_user_func($this->factory);
            }

            public function isShared(): bool {
                return $this->shared;
            }
        } );
    }

    /**
     * Return the list of service identifiers that have been registered
     */
    public function getServiceIdentifiers(): array {
        return $this->identifiers;
    }

    /**
     * Return the map of aliases as an associative array
     */
    public function getAliases(): array {
        return $this->aliases;
    }

    /**
     * Register the identifier that we added to the backend container,
     * and remove it as an alias, if it has previously been added.
     *
     * @param string $id The package id
     */
    protected function registerIdentifier( string $id ): void {
        $this->identifiers[$id] = $id;
        unset($this->aliases[$id]);
    }

    /**
     * If `$id` is a class name, then this function will find all parent
     * classes and interfaces and add them as aliases.
     *
     * @param string $id The package id
     */
    protected function discoverAliases( string $id, string $className=null ): void {
        if ($className === null && (class_exists($id) || interface_exists($id))) {
            $className = $id;
        } else {
            die("No aliases can be found $id=$className");
        }

        $queue = [ new \ReflectionClass($className) ];

        /**
         * Scan the dependency graph using a queue
         */
        while($next = array_shift($queue)) {
            if( $parent = $next->getParentClass() ) {
                // Register a mapping from parent class name to the current class name and enqueue the parent
                $this->aliases[$parent->getName()][] = $next->getName();
                $queue[] = $parent;
            }
            foreach($next->getInterfaces() as $interface) {
                // Register a mapping from interfaces to the class and enqueue the interface
                $this->aliases[$interface->getName()][] = $next->getName();
                $queue[] = $interface;
            }
        }
    }

    /**
     * Instance cache for containers
     */
    protected static $instances = [];

    /**
     * @see ContainerInterface::getInstance()
     */
    public static function getInstance(string $pathClassOrNamespace=null): Container {
        /**
         * Resolve namespaces
         */
        $config = F2::config('container/services', []);

        /**
         * Optimization: Most people will not use a namespaced service container, or we already have the namespace provided
         */
        if ( ($pathClassOrNamespace !== null && isset($config[$pathClassOrNamespace])) || sizeof($config) === 1 ) {
            $pathClassOrNamespace = $pathClassOrNamespace === null ? '' : $pathClassOrNamespace;
            if (!isset(static::$instances[''])) {
                $container = new static();
                foreach ($config[''] as $spec) {
                    $container->addFactoryFromSpecification($spec);
                }
                return static::$instances[''] = $container;
            }
            return static::$instances[''];
        }

        /**
         * If no namespace is declared, we'll try to detect it.
         */
        if ($pathClassOrNamespace === null) {
            // Must try parent paths and namespaces, unless config only has root namespace
            $callingContext = static::getCallingContext();

            // First test the calling namespace/class
            if ("" !== ($resolvedTo = static::resolveLiteral($callingContext['literal']))) {
                return static::getInstance($resolvedTo);
            }

            // Finally use path resolution, which ends up with "" - the default namespace
            $resolvedTo = static::resolvePath($callingContext['path']);
            return static::getInstance($resolvedTo);
        } else {
            if (
                class_exists($pathClassOrNamespace, false) ||
                is_callable($pathClassOrNamespace)
            ) {
                // This is a literal
                return static::getInstance(static::resolveLiteral($pathClassOrNamespace));
            } else if (file_exists($pathClassOrNamespace)) {
                // This is a path
                return static::getInstance(static::resolvePath($pathClassOrNamespace));
            } else {
                // Assume it is a namespace
                return static::getInstance(static::resolveLiteral($pathClassOrNamespace));
            }
        }
    }

    /**
     * See which config exists based on class/namespace name
     */
    protected static function resolveLiteral( string $literal ) : string {
        $config = F2::config('container/services', []);

        while ($literal !== "" && !isset($config[$literal])) {
            $literal = implode("\\", array_slice(explode("\\", $literal), 0, -1));
        }

        return $literal;
    }

    /**
     * See which config exists based on path
     */
    protected static function resolvePath( string $path ) : string {
        $config = F2::config('container/services', []);

        $path = str_replace("\\", "/", $path);

        while (!isset($config[$path]) && $path !== ".") {
            $path = dirname($path);
        }

        if ($path === ".") {
            $path = "";
        }

        return $path;
    }

    /**
     * Inspect the call stack to determine which function or namespace that called
     * getInstance()
     */
    protected static function getCallingContext():array {
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 10);
        $root = F2::config('ROOT');
        $rootLength = strlen($root);

        // Searching for these:
        $foundPath = null; // relative to root
        $literal = ''; // root namespace is default

        $i = -1;
        while (isset($trace[++$i])) {
            // Ignore calls from this file
            if ($trace[$i]['file'] === __FILE__) {
                continue;
            }

            $path = $trace[$i]['file'];

            // Ignore calls from the preloader class
            $reflector = new \ReflectionClass('F2');
            if ($path === $reflector->getFileName()) {
                continue;
            }

            // Make paths relative if inside $root
            if (substr($path, 0, $rootLength) === $root) {
                $path = substr($path, $rootLength + 1);
            }

            // Record the path result
            if ($foundPath === null) {
                $foundPath = $path;
            }

            if (isset($trace[$i]['class']) && $trace[$i]['class'] === 'F2') {
                continue;
            }

            if (isset($trace[$i]['class'])) {
                $literal = $trace[$i]['class'];
                break;
            } elseif (isset($trace[$i]['function'])) {
                $literal = $trace[$i]['function'];
                break;
            }
        }
        // Root namespace assumed if we can't find a namespace yet
        return ['path' => $foundPath, 'literal' => $literal];;
    }

}
