<?php
namespace F2\Container;

use Psr\Container\ContainerInterface as PsrContainerInterface;

interface ContainerInterface extends PsrContainerInterface {

    /**
     * Adds a generic factory
     */
    public function addFactory( AbstractFactory $factory ): void;

    /**
     * Adds a .php file factory that will provide the registered class when included.
     *
     * Example: $container->addFileFactory( 'some/file.php', GuzzleHttp\Client::class, true)
     *
     * @param $path         Path to the file that will return an instance
     * @param $className    Class name that this file returns.
     * @param $shared       If the instance should be cached and shared among consumers.
     */
    public function addFileFactory( string $path, $identifier, bool $shared=false ): void;

    /**
     * Add a callable factory that will provide the type hinted class
     *
     * Example: $container->addCallableFactory( function(): \GuzzleHttp\Client { .. code .. }, true );
     *
     * @param $shared       If the instance should be cached and shared among consumers.
     * @param $className    If the callable does not type hint, you can override the return type.
     */
    public function addCallableFactory( callable $factory, $identifier, bool $shared=false ): void;

    /**
     * Convenience method for creating a service from a service specification array. A
     * service specification array has the following form:
     *
     * ```php
     * [ $type, $typeParameter, $identifier, $shared ]
     * ```
     *
     * $type must be one of 'callable', 'config', 'factory', 'file'
     *
     * $typeParameter must be:
     *     callable:    an array or a string containing a callable (not a closure),
     *     config:      a configuration key,
     *     factory:     a factory class name,
     *     file:        a file path (relative paths are resolved from APP_ROOT)
     *
     * Examples:
     *
     * ```php
     * $spec = [ 'callable', [ 'MyPDOServiceProvider', 'getInstance' ], \PDO::class, true ]
     * $spec = [ 'config', 'my-service', 'MyServiceIdentifier', true ]
     * $spec = [ 'factory', MyFactoryClass::class, MyInterface::class, false ]
     * $spec = [ 'file', 'app/src/myServiceFactory.php', MyServiceInterface::class, true ]
     * ```
     *
     * @param array $specification The service specification array
     */
    public function addFactoryFromSpecification( array $spec ): void;

    /**
     * Get all registered service identifiers
     */
    public function getServiceIdentifiers(): array;

    /**
     * Get all declared aliases as an associative array
     */
    public function getAliases(): array;

    /**
     * Delegate a backup container to be checked for services if it
     * cannot be resolved via this container.
     *
     * @param \Psr\Container\ContainerInterface $container
     */
    public function delegate(PsrContainerInterface $container): void;

    /**
     * Retrieve the container instance for a given namespace. If no namespace is provided
     * autodetect the namespace using debug_backtrace(). No need to autodetect namespace
     * if no containers have been defined for specific namespaces.
     */
    public static function getInstance(string $namespace=''): Container;
}
