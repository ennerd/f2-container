<?php
WORK IN PROGRESS. SHOULD BE REFACTORED AND SEPARATED OUT INTO FUNCTIONS:

Look into how Symfony does auto wiring.

namespace F2\Container;

use ReflectionClass;
use ReflectionMethod;

trait InjectorTrait {

    /**
     * Create a new instance by using reflection to resolve arguments and properties
     */
    public function instantiate(string $className) {
        $args = [];

        $rc = new ReflectionClass($className);
        return $rc->newInstanceArgs($this->createArguments($rc->getConstructor()));
    }

    /**
     * Invoke the callback by using reflection to resolve arguments
     */
    public function invoke(callable $callable) {
        $rm = new ReflectionMethod($callable);
        return $rm->invokeArgs($this->createArguments($rm));
    }

    public function inject(object $ob) {
        $ro = new ReflectionObject($ob);
        foreach($ro->getProperties() as $rp) {
            if ($rp->isStatic()) {
                continue;
            }
            if (!$rp->isInitialized($ob)) {
                
            }
        }
    }

    /**
     * Find arguments
     */
    protected function createArguments(ReflectionMethod $rm) {
        $params = $rc->getConstructor()->getParameters();
        foreach ($params as $param) {
            if ($param->hasType() && $this->has($param->getType())) {
                $args[] = $this->get($param->getType());
            } elseif ($param->isDefaultValueAvailable()) {
                $args[] = $param->getDefaultValue();
            } elseif ($param->isOptional()) {
                $args[] = null;
            } elseif ($param->hasType()) {
                throw new Exception("Mandatory parameter '\$".$param->getName()."' has no default value.");
            } else {
                throw new Exception("Mandatory parameter '\$".$param->getName()."' has no type hint.");
            }
        }
        return $args;
    }

}
