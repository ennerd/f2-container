<?php
namespace F2\Container;

use Psr\Container\NotFoundExceptionInterface;

class NotFoundException extends LogicException {}
