<?php
declare(strict_types=1);

namespace F2\Container;

use League\Container\ServiceProvider\AbstractServiceProvider as LeagueAbstractServiceProvider;
use League\Container\ServiceProvider\BootableServiceProviderInterface;

abstract class AbstractServiceProvider extends LeagueAbstractServiceProvider implements BootableServiceProviderInterface {
    /**
     * Override this method to perform setup tasks early. Use this with care, since it is a waste
     * of resources to prepare stuff that might not be used.
     *
     * Method will be invoked on registration of a service provider implementing
     * this interface. Provides ability for eager loading of Service Providers.
     *
     * @return void
     */
    public function boot() {
    }

    abstract public function register();
}
