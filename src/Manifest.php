<?php
declare(strict_types=1);

namespace F2\Container;

use F2\Common\AbstractManifest;
use Psr\Container\ContainerInterface;

class Manifest extends AbstractManifest {

    public function getDependencies(): iterable {
        return [
            \F2\Common\Manifest::class,
        ];
    }

    public function getF2Methods(): iterable {
        // Can't use service provider for this.
        yield "container"  => [\F2\Container\Container::class, 'getInstance'];
    }

    public function registerConfigDefaults(array $config): array {
        // Add the container as a default service
        $config['container/services'][] = [ 'class', Container::class, ContainerInterface::class, true ];
        return $config;
    }

}

